#!/bin/bash
set -euxo pipefail

export DEBFULLNAME="WMCS GitLab Debian autobuilder"
export DEBEMAIL="cloud-admin@lists.wikimedia.org"

# shellcheck disable=SC1091
DEBIAN_CODENAME=$(. /etc/os-release; echo "$VERSION_CODENAME")

echo """
Types: deb deb-src
URIs: https://mirrors.wikimedia.org/debian
Suites: ${DEBIAN_CODENAME} ${DEBIAN_CODENAME}-updates
Components: main
""" | sudo tee /etc/apt/sources.list.d/debian.sources

echo """
Types: deb deb-src
URIs: http://security.debian.org/debian-security
Suites: ${DEBIAN_CODENAME}-security
Components: main
""" | sudo tee /etc/apt/sources.list.d/debian-security.sources

# Empty out the main sources.list
sudo rm /etc/apt/sources.list
sudo touch /etc/apt/sources.list

# Remove backports from the default config, unless enabled
if [[ -v ENABLE_BACKPORTS && -n "$ENABLE_BACKPORTS" ]]; then
	# we have to make the backports as priorary as the rest so it will install packages from it if
	# there's newer versions. By default they get 100 and the others 500
	echo """
Package: *
Pin: release a=${DEBIAN_CODENAME}-backports
Pin-Priority: 500
	""" | sudo tee /etc/apt/preferences.d/backports

	echo """
Types: deb deb-src
URIs: https://mirrors.wikimedia.org/debian
Suites: ${DEBIAN_CODENAME}-backports
Components: main
# make sure it has the same priority as the rest, otherwise
# packages from here get lower priority by default
Pin-Priority: 500
""" | sudo tee /etc/apt/sources.list.d/debian-backports.sources
fi

if [[ -v ENABLE_TOOLFORGE_REPO && -n "$ENABLE_TOOLFORGE_REPO" ]]; then
	echo """
Types: deb
URIs: https://deb-tools.wmcloud.org/repo/
Suites: ${DEBIAN_CODENAME}-toolsbeta
Components: main
Trusted: yes
""" | sudo tee /etc/apt/sources.list.d/toolforge.sources
fi

sudo apt-get update

# aaargh
git config --global --add safe.directory "$PWD"

# Use Aptitude, as it does a better job at resolving packages
# from backports etc.
mk-build-deps debian/control --install --root-cmd sudo --tool "aptitude -y"
# Remove build-dep files
git clean -fdx

mkdir debs
gbp buildpackage --no-sign --git-export-dir=debs
