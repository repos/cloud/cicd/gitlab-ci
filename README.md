# gitlab-ci

WMCS repository for Gitlab CI/CD related stuff.

You can use it by adding the following to your `.gitlab-ci.yml`.

# Python-based projects

## Simple tox

```
include:
  - project: "repos/cloud/cicd/gitlab-ci"
    file: "py3.11-bookworm-tox/gitlab-ci.yaml"
```

## Tox + Debian package + pypi publish

```
include:
  - project: "repos/cloud/cicd/gitlab-ci"
    file: "py3.11-bookworm-tox-pypi-debian/gitlab-ci.yaml"
```

### Set up PyPI credentials

To setup the PyPI credentials:

- In the PyPI project, go to Manage project, then select Settings and click on
  "Create a token for [project]". Create a token restricted to that project.
- Also from the same PyPI settings screen, enable the 2FA requirement for that
  project. (This is not strictly necessary, but feels like a best practice to
  follow.)
- In GitLab, go to Settings, select Repository from the settings sidebar menu,
  and under Protected branches set the main branch as protected.
- In that same page, and under Protected tags set the tag pattern (ex.
  `debian/*`) matching the release tags as protected.
- In the CI/CD settings setion, create two variables:
  - `TWINE_USERNAME`: this will always be `__token__`.
  - `TWINE_PASSWORD`: the API token you created. This needs to be set as
    Protected and Masked. ``

## Tox + Debian package

```
include:
  - project: "repos/cloud/cicd/gitlab-ci"
    file: "py3.11-bookworm-tox-debian/gitlab-ci.yaml"
```

## Toolforge components

### All

```
include:
  - project: "repos/cloud/cicd/gitlab-ci"
    file: "toolforge-cd/test_and_image_and_helm.yaml"

variables:
  # this will be the name for the image and the chart, might not be the name
  # of the repository
  PROJECT_NAME: builds-api
```

That will add:

- pre-commit
- run the blubber 'test' variant
- Publish a dev chart + image to toolsbeta for a merge request
- Publish a chart + image to toolsbeta for pushes
- Publish a chart + image to tools for pushes manually gated (you have to
  manually click for it to deploy)
- Create a gitlab release
- Create a merge request on toolforge-deploy repo

It will expect to find the chart for the project under
`deployment/chart/Chart.yaml`.

It expects to be able to build the image from the blubberfile
`.pipeline/blubber.yaml`.

### Golang/memory eager jobs

If the component needs extra memory (like `golanglint-ci`) you can add the
following to the `.gitlab-ci.yml` file to make it run on memory-opmitized
runners:

```
variables:
    RUNNER_TAG: "memory-optimized"
```

# Building the images

Currently all these images live in `docker-registry.tools.wmflabs.org`, so to
build them you have to:

```
mylaptop:~$ ssh tools-imagebuilder-2.tools.eqiad1.wikimedia.cloud  # or whichever is the current image builder host
dcaro@tools-imagebuilder-2:~$ sudo -i
root@tools-imagebuilder-2:~$ [[ -d gitlab-ci ]] || git clone https://gitlab.wikimedia.org/repos/cloud/cicd/gitlab-ci.git  # If it's not there
root@tools-imagebuilder-2:~# cd gitlab-ci/
root@tools-imagebuilder-2:~/gitlab-ci# git fetch --all && git reset --hard FETCH_HEAD
```

And for the image you want to build, use a tag like
`cloud-cicd-<dirname>:latest`, where `<dirname>` is the directory containing the
`Dockerfile`, for example for `py3.11-bookworm-tox`:

```
root@tools-imagebuilder-2:~/gitlab-ci# cd py3.11-bookworm-tox
root@tools-imagebuilder-2:~/gitlab-ci/py3.11-bookworm-tox# docker build --no-cache . -t docker-registry.tools.wmflabs.org/cloud-cicd-py3.11-bookworm-tox:latest
```

And then push:

```
root@tools-imagebuilder-2:~/gitlab-ci/py3.11-bookworm-tox# docker push docker-registry.tools.wmflabs.org/cloud-cicd-py3.11-bookworm-tox:latest
```

# Pre-commit hooks

## check-openapi-version-bump

This hook will check that if the `openapi/openapi.yaml` file changed, it also
bumped the version in it.

To use add this to your `.pre-commit-config.yaml`:

```
  - repo: https://gitlab.wikimedia.org/repos/cloud/cicd/gitlab-ci rev:
    run_pre-commit_autoupdate_to_fix_me hooks:
    - id: check-openapi-version-bump
```

And then run the autoupdate to get the latest commit:

```
pre-commit autoupdate
```
