#!/bin/bash

set -o nounset
set -o errexit
set -o pipefail

OPENAPI_FILE_RELPATH="openapi/openapi.yaml"


have_non_commited_changes() {
    git status --porcelain | grep -q "$OPENAPI_FILE_RELPATH"
}

have_commited_changes() {
    git show --stat | grep -q "^ *$OPENAPI_FILE_RELPATH"
}


is_greater_version() {
    local left_version="${1?}"
    local right_version="${2?}"
    local highest_version

    if [[ "$left_version" == "$right_version" ]]; then
        return 1
    fi

    highest_version="$(echo -e "$left_version\n$right_version" | sort --version-sort | tail -n1)"
    if [[ "$left_version" == "$highest_version" ]]; then
        return 0
    fi
    return 1
}


main() {
    local diff=""
    # this happens when running as a hook
    if have_non_commited_changes; then
        echo "Checking local changes"
        diff+="\n$(git diff "$OPENAPI_FILE_RELPATH")"
        diff+="\n$(git diff --cached "$OPENAPI_FILE_RELPATH")"
    # this happens when forcing a run (pre-commit run -a)
    elif have_commited_changes; then
        echo "Checking last commit changes"
        diff+="\n$(git show -- "$OPENAPI_FILE_RELPATH")"
    fi

    if [[ "$diff" == "" ]]; then
        echo "No changes to the api found - OK"
    elif echo "$diff" | grep -q -e "^+ *version:"; then
        old_version="$(echo "$diff" | grep -e '^- *version:' | grep -o -e '[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+')"
        new_version="$(echo "$diff" | grep -e '^+ *version:' | grep -o -e '[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+')"
        if [[ "$old_version" == "" ]] || [[ "$new_version" == "" ]]; then
            echo "Unable to find the versions to compare, make sure they are in the form X.Y.Z with X, Y and Z being digits"
        fi
        if ! is_greater_version "$new_version" "$old_version"; then
            echo "The new version ($new_version) is not greater than the old version ($old_version), please make sure to increase it."
            return 1
        fi
        echo "Found changes to the api, but found also version bump (from $old_version -> $new_version) - OK"
    else
        echo "Found changes to the api that did not bump the version, please bump (info.version)."
        return 1
    fi
    return 0
}


main "$@"
