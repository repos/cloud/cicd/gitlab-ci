#!/bin/bash

set -o nounset
set -o errexit
set -o pipefail


# leaving here to make it clear it's needed
GITLAB_REPO_PREFIX="${GITLAB_REPO_PREFIX?}"
AUTOUPDATE_BRANCH="poetry_autoupdate"


update_repo() {
    local repo="${1?}"
    local url="$GITLAB_REPO_PREFIX/${repo}"
    local dir="/tmp/$repo"

    rm -rf "$dir"

    git config --global user.email "ci-bot@toolforge.org"
    git config --global user.name "Toolforge CI bot"

    git clone --depth 1 "$url" "$dir"
    if ! [[ -f "$dir/poetry.lock" ]]; then
        echo "Repo $repo has no poetry.lock file, skipping"
        return 0
    fi

    cd "$dir"
    git checkout -b "$AUTOUPDATE_BRANCH"
    git reset --hard "origin/$AUTOUPDATE_BRANCH" 2>/dev/null \
    && echo "Branch already exists, reusing..." \
    || echo "Creating new branch..."

    poetry update
    if [[ "$(git status --short)" == "" ]]; then
        echo "Repo $repo already up to date."
        cd -
        return 0
    fi
    git commit -a -m 'poetry: Autoupdate'

    git push "origin" "HEAD:$AUTOUPDATE_BRANCH" \
        --force \
        -o merge_request.create \
        -o merge_request.label="Needs review"

    echo "####### Opened new MR for $repo"

    cd -
}


get_all_repos() {
    # repos/cloud/toolforge project is id 203 (from gitlab group web ui)
    curl --silent 'https://gitlab.wikimedia.org/api/v4/groups/203/projects?per_page=1000' \
    | jq -r '.[] | select(.archived == false) | .path'
}



main() {
    local all_repos
    # this on it's own to not hide the return code
    all_repos="$(get_all_repos)"
    # one line per element
    mapfile -t all_repos <<<"$all_repos"
    for repo in "${all_repos[@]}"; do
        update_repo "${repo}"
    done
}


main "$@"
