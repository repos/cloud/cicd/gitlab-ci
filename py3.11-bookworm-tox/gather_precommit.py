#!/usr/bin/env python3
"""
Use this script to generate the pre-commit config to cache the versions of hooks we use.

./gather_precommit.py > to-cache-pre-commit-config.yaml
"""
import sys
import requests
import http
import yaml
import datetime

GITLAB_PREFIX = "https://gitlab.wikimedia.org/repos/cloud/toolforge"


def get_all_repos():
    # repos/cloud/toolforge project is id 203 (from gitlab group web ui)
    response = requests.get(
        "https://gitlab.wikimedia.org/api/v4/groups/203/projects?per_page=1000"
    )
    return [repo["path"] for repo in response.json()]


def main() -> None:
    all_repos = {}
    for repo in get_all_repos():
        # this expects main to be the default branch
        mabye_config = requests.get(
            f"{GITLAB_PREFIX}/{repo}/-/raw/main/.pre-commit-config.yaml?ref_type=heads",
            headers={
                "User-Agent": "gather_precommit_script @gitlab.wikimedia.org/repos/cloud/cicd/gitlab-ci"
            },
        )
        if mabye_config.status_code == http.HTTPStatus.OK:
            print(f"Repo {repo} has config, parsing...", file=sys.stderr)
            new_config = yaml.safe_load(mabye_config.text)
            for new_repo in new_config["repos"]:
                if "rev" not in new_repo:
                    print(
                        f"Skipping pre-commit repo {new_repo} as there's no version defined (maybe a manual hook)",
                        file=sys.stderr,
                    )
                    continue

                print(
                    f"Found pre-commit repo {new_repo} adding to the list...",
                    file=sys.stderr,
                )
                repo_key = f"{new_repo['repo']}-{new_repo['rev']}"
                all_repos[repo_key] = new_repo
        else:
            print(
                f"Repo {repo} does not seem to have a pre-commit config file (.pre-commit-config.yaml), skipping",
                file=sys.stderr,
            )

    print(yaml.safe_dump({"repos": list(all_repos.values())}))
    print(f"# Generated with {__file__} on {datetime.datetime.now()}")


if __name__ == "__main__":
    main()
