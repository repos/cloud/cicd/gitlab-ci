# Requirements:
# * blubberfile under `.pipeline/blubber.yaml` with an `image` variant
# see  https://wikitech.wikimedia.org/wiki/Wikimedia_Cloud_Services_team/EnhancementProposals/Decision_record_T339198_Toolforge_component_deployment_flow_details
include:
  - project: "repos/cloud/cicd/gitlab-ci"
    file: "toolforge-cd/stages.yaml"
  - project: "repos/releng/kokkuri"
    file: "includes/images.yaml"
  - project: "repos/cloud/cicd/gitlab-ci"
    file: "toolforge-cd/version_generation.yaml"
  - project: "repos/cloud/cicd/gitlab-ci"
    file: "toolforge-cd/create_release.yaml"
  - project: "repos/cloud/cicd/gitlab-ci"
    file: "toolforge-cd/create_toolforge_deploy_mr.yaml"

variables:
  # something like envvars-api or builds-api, will be the repo name in harbor
  PROJECT_NAME: CHANGEME!

# hidden job starting with `.` to declare yaml anchors to use later
.definitions:
  commands:
    - &add-toolsbeta-auth |
      mkdir -p ~/.docker || : \
      && echo "{\"auths\":{\"toolsbeta-harbor.wmcloud.org\":{\"auth\":\"$TOOLSBETA_HARBOR_AUTH\"}}}" \
      > ~/.docker/config.json

    - &add-tools-auth |
      mkdir -p ~/.docker || : \
      && echo "{\"auths\":{\"tools-harbor.wmcloud.org\":{\"auth\":\"$TOOLS_HARBOR_AUTH\"}}}" \
      > ~/.docker/config.json

    - &build-and-publish |
      tag=$(cat image-version.txt)
      buildctl \
        --timeout $CI_JOB_TIMEOUT \
        --wait \
        build \
        --progress=plain \
        --frontend=gateway.v0 \
        --opt source=docker-registry.wikimedia.org/repos/releng/blubber/buildkit:v0.21.1 \
        --local context=. \
        --local dockerfile=. \
        --opt filename=.pipeline/blubber.yaml \
        --opt target=image \
        --output "type=image,name=${KOKKURI_REGISTRY_PUBLIC}/toolforge/${PROJECT_NAME}:${tag},push=true"

publish-devimage-toolsbeta:
  extends: .kokkuri:build-and-publish-image
  # has to be before publish
  stage: publish
  # cloud runners have more disk space, so we get less errors when building images
  # Waiting for https://phabricator.wikimedia.org/T340841 to get fixed, it's better to fail sometimes than to fail always
  # tags:
  #   - cloud
  variables:
    BUILD_VARIANT: image
    REPOSITORY: toolsbeta
    KOKKURI_REGISTRY_PUBLIC: $REPOSITORY-harbor.wmcloud.org
  dependencies:
    - generate-versions-devel
  script:
    - *add-toolsbeta-auth
    - *build-and-publish
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

publish-image-toolsbeta:
  extends: .kokkuri:build-and-publish-image
  stage: deployment
  # cloud runners have more disk space, so we get less errors when building images
  # Waiting for https://phabricator.wikimedia.org/T340841 to get fixed, it's better to fail sometimes than to fail always
  # tags:
  #   - cloud
  environment:
    name: staging
    url: https://toolsbeta-harbor.wmcloud.org/harbor/projects/toolforge/repositories/$PROJECT_NAME/artifacts-tab/artifacts
  variables:
    BUILD_VARIANT: image
    REPOSITORY: toolsbeta
    KOKKURI_REGISTRY_PUBLIC: $REPOSITORY-harbor.wmcloud.org
  dependencies:
    - generate-versions-prod
  script:
    - *add-toolsbeta-auth
    - *build-and-publish

  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

publish-image-tools:
  extends: .kokkuri:build-and-publish-image
  stage: deployment
  # cloud runners have more disk space, so we get less errors when building images
  # Waiting for https://phabricator.wikimedia.org/T340841 to get fixed, it's better to fail sometimes than to fail always
  # tags:
  #   - cloud
  environment:
    name: production
    url: https://tools-harbor.wmcloud.org/harbor/projects/toolforge/repositories/$PROJECT_NAME/artifacts-tab/artifacts
  variables:
    BUILD_VARIANT: image
    REPOSITORY: tools
    KOKKURI_REGISTRY_PUBLIC: $REPOSITORY-harbor.wmcloud.org
  dependencies:
    - generate-versions-prod
  script:
    - *add-tools-auth
    - *build-and-publish
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

# prevent running the workflows in any other case but the following
workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED
